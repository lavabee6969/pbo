# 1. Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital
**- Spotify Use Case User**
![](https://gitlab.com/lavabee6969/pbo/-/raw/main/UAS%20PBO/Screenshot_2023-07-11_193210.png)

**- Spotify Use Case Manajemen Perusahaan**
![](https://gitlab.com/lavabee6969/pbo/-/raw/main/UAS%20PBO/Screenshot_2023-07-11_193230.png)

**- Spotify Use Case Direksi Perusahaan**
![](https://gitlab.com/lavabee6969/pbo/-/raw/main/UAS%20PBO/Screenshot_2023-07-11_193411.png)

# 2. Mampu mendemonstrasikan Class Diagram dari keseluruhan Use Case produk digital
![](https://gitlab.com/lavabee6969/pbo/-/raw/main/UAS%20PBO/Screenshot_2023-07-11_195124.png)

# 3. Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle
Dalam pembuatan Produk Digital Spotify ini menggunakan web service RESTful API dengan bahasa **TypeScript** dan **library dari Adonis JS**. Dengan menerapkan beberapa point SOLID Design Principle kedalam source code controller, seperti berikut :

```
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import User from 'App/Models/User'

export default class AuthController {
    public async register({ request, response }: HttpContextContract) {
        try {
          const registerValidation = schema.create({
            email: schema.string({}, [
              rules.email(),
              rules.unique({
                table: "users",
                column: "email"
              })
            ]),
            password: schema.string({}, [
              rules.minLength(6)
            ]),
            username: schema.string()
            
          })
          const payload = await request.validate({ schema: registerValidation })
          await User.create(payload)
    
          return response.created({
            messsage: "register berhasil"
          })
        } catch (error) {
          return response.badRequest({
            massage: error.message
          })
        }
      }

    public async login({ request, response, auth }: HttpContextContract) {
        try {
          const loginValidation = schema.create({
            email: schema.string(),
            password: schema.string()
          })
          await request.validate({ schema: loginValidation })
    
          const email = request.input('email')
          const password = request.input('password')
    
          const token = await auth.use('api').attempt(email, password, {
            expiresIn: '1 Days'
          })
    
          return response.ok({
            message: 'Login successful',
            token
          })
        } catch (error) {
          console.error("Error:", error)
          return response.unauthorized({
            message: 'Login failed'
          })
        }
      }
}
```

Penjelasan SOLID DEsign Principle :
1. Single Responsibillity Principle (SRP)
Dalam kelas AuthController, setiap method bertanggung jawab atas "register" yang berfungsi untuk mendaftarkan user dan "login" yang berfungsi untuk mengautentikasi inputan dari user.
2. Dependency Inversion Principle (DIP)
Implementasi DIP terdapat pada method "register" yang bergantung pada "user" model yang diimpor dari "App/Models/User" dengan depedensi yang diimpor melalui string literal dan tidak secara langsung terikat oleh kelas "AuthController".

Kemudian pada "HttpContextContract", "request", "response", dan "auth" juga terdapat depedensi yang diinjeksi melalui method yang fleksibel.

# 4. Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih
```
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import User from 'App/Models/User'

export default class AuthController {
    public async register({ request, response }: HttpContextContract) {
        try {
          const registerValidation = schema.create({
            email: schema.string({}, [
              rules.email(),
              rules.unique({
                table: "users",
                column: "email"
              })
            ]),
            password: schema.string({}, [
              rules.minLength(6)
            ]),
            username: schema.string()
            
          })
          const payload = await request.validate({ schema: registerValidation })
          await User.create(payload)
    
          return response.created({
            messsage: "register berhasil"
          })
        } catch (error) {
          return response.badRequest({
            massage: error.message
          })
        }
      }

    public async login({ request, response, auth }: HttpContextContract) {
        try {
          const loginValidation = schema.create({
            email: schema.string(),
            password: schema.string()
          })
          await request.validate({ schema: loginValidation })
    
          const email = request.input('email')
          const password = request.input('password')
    
          const token = await auth.use('api').attempt(email, password, {
            expiresIn: '1 Days'
          })
    
          return response.ok({
            message: 'Login successful',
            token
          })
        } catch (error) {
          console.error("Error:", error)
          return response.unauthorized({
            message: 'Login failed'
          })
        }
      }
}
```

Berdasarkan source code pada kelas "AuthController" dalam mengatur logika web service, terdapat penerapan konsep design pattern, yang terdiri dari :
1. Model View Controller (MVC)
Dengan menerapkan konsep MVC pada kelas "AuthController" sebagai bagian dari lapisan controller. Kemudian method "register" dan "login" untuk menerima request dari user lalu diproses dan mengembalikan respon.

2. Front Controller
Dalam implementasi konsep Front Controller, kelas "AuthController" menerima request dari user dengan method yang sesuai untuk memproses request.

3. Data Validation 
Dengan menginport dari library "import { schema, rules } from '@ioc:Adonis/Core/Validator'" digunakan dalam methhod "register" dan "login" untuk proses validasi data dari inputan atau request user.

# 5. Mampu menunjukkan dan menjelaskan konektivitas ke database
1. Mengaktifkan server web 

![](https://gitlab.com/lavabee6969/pbo/-/raw/main/UAS%20PBO/Screenshot_2023-07-16_004926.png)
Memulai http server dengan perintah "npm run dev" pada terminal Visual Studio Code kemudian seluruh library pada Adonis JS akan membuat server address yang nanti nya server address tersebut akan digunakan untuk perintah POST atau request pada frame work pengujian web service.

2. Membuat request input oleh user 
![](https://gitlab.com/lavabee6969/pbo/-/raw/main/UAS%20PBO/Screenshot_2023-07-16_004710.png)
Menggunakan server address yang sudah dibuat, kemudian memasukkan data atau perintah field inputan sesuai dengan yang sudah dibuat pada tabel database yaitu dengan menginput atau memberikan request berupa "email", "password", "username" pada "register" dan request "email", "password" pada "login" menggunakan Frame work "Postman" untuk menguji dan menampung web service.

3. Request User akan terkoneksi oleh database
![](https://gitlab.com/lavabee6969/pbo/-/raw/main/UAS%20PBO/Screenshot_2023-07-16_005614.png)
Kemudian setelah field data di POST, maka akan masuk kedalam database phpmyadmin. "users" untuk menampung data "register" dan database "api_tokens" untuk menampung data login.

# 6. Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya
1. Proses Pembuatan Web service
```
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import User from 'App/Models/User'

export default class AuthController {
    public async register({ request, response }: HttpContextContract) {
        try {
          const registerValidation = schema.create({
            email: schema.string({}, [
              rules.email(),
              rules.unique({
                table: "users",
                column: "email"
              })
            ]),
            password: schema.string({}, [
              rules.minLength(6)
            ]),
            username: schema.string()
            
          })
          const payload = await request.validate({ schema: registerValidation })
          await User.create(payload)
    
          return response.created({
            messsage: "register berhasil"
          })
        } catch (error) {
          return response.badRequest({
            massage: error.message
          })
        }
      }

    public async login({ request, response, auth }: HttpContextContract) {
        try {
          const loginValidation = schema.create({
            email: schema.string(),
            password: schema.string()
          })
          await request.validate({ schema: loginValidation })
    
          const email = request.input('email')
          const password = request.input('password')
    
          const token = await auth.use('api').attempt(email, password, {
            expiresIn: '1 Days'
          })
    
          return response.ok({
            message: 'Login successful',
            token
          })
        } catch (error) {
          console.error("Error:", error)
          return response.unauthorized({
            message: 'Login failed'
          })
        }
      }
}
```
Produk Digital Spotify ini dibuat menggunakan web service dengan bantuan Library Adonis JS menggunakan Bahasa TypeScript. Terdapat beberapa library yang digunakan. Library @ioc:Adonis/Core/HttpContext digunakan untuk mengakses permintaan dan respons HTTP, library @ioc:Adonis/Core/Validator digunakan untuk melakukan validasi data. Library 'App/Models/User' adalah model pengguna yang memberikan akses ke operasi-operasi terkait pengguna seperti membuat pengguna baru.

Web service dibuat dengan dua endpoint, yaitu "/register" untuk mendaftarkan user baru dan "/login" untuk melakukan autentikasi input user. Endpoint "/register" digunakan untuk menerima data user baru, melakukan validasi, dan menyimpannya ke dalam database. 

Endpoint "/login" digunakan untuk menerima data autentikasi user, melakukan validasi, dan menghasilkan token akses jika autentikasi berhasil. Kemudian web service akan memberikan respons dengan pesan yang sesuai tergantung pada hasil operasi yang dilakukan, seperti berhasil mendaftar atau gagal melakukan login.

2. Operasi CRUD
- CREATE (Register)
Proses Create data pada web service dimulai dengan menggunakan method "register" yang berfungsi untuk membuat user dengan mengirimkan permintaan HTTP POST ke endpoint. Data inputan user ("email", "password", "username") akan dikirimkan sebagai data masukkan. Data akan di validasi menggunakan library atau skema "@ioc:Adonis/Core/Validator". Jika berhasil membuat user baru, akan muncul respon dengan status HTTP 201 Created dan menampilkan pesan berhasil.

-READ (Autentikasi)
Proses Read data pada web service dimulai dengan mengirimkan request HTTP POST ke endpoint untuk autentikasi user menggunakan method "login". Data inputan user ("email", "password") akan dikirimkan sebagai data masukkan. Data akan di validasi menggunakan library atau skema "@ioc:Adonis/Core/Validator". Setelah proses validasi sukses, data akan melakukan autentikasi menggunakan "auth.use('api').attempt(email, password)". Jika autentikasi berhasil, akan menghasilkan token akses dan muncul respon dengan status HTTP 200 OK dan pesan berhasil, jika gagal, akan muncul respon dengan status HTTP 401 unauthorized dengan pesan gagal.

# 7. Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital
1. GUI Register
![](https://gitlab.com/lavabee6969/pbo/-/raw/main/UAS%20PBO/Screenshot_2023-07-16_023759.png)
Pada bagian Register terdapat beberapa field untuk user menginputkan data, yang terdiri dari :
- username
- email
- password

2. GUI Login
![](https://gitlab.com/lavabee6969/pbo/-/raw/main/UAS%20PBO/Screenshot_2023-07-16_023707.png)
Pada bagian Login terdapat beberapa field untuk user menginputkan data, yang terdiri dari :
- email
- password

# 8. Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital
```
// Ambil elemen-elemen yang diperlukan dari halaman HTML
const loginPage = document.getElementById('login-page');
const registerPage = document.getElementById('register-page');
const loginForm = document.getElementById('login-form');
const registerForm = document.getElementById('register-form');
const loginLink = document.getElementById('login-link');
const registerLink = document.getElementById('register-link');

// Tampilkan halaman login saat halaman dimuat
document.addEventListener('DOMContentLoaded', () => {
  showLoginPage();
});

// Tambahkan event listener untuk tombol Login
loginForm.addEventListener('submit', (event) => {
  event.preventDefault();
  loginUser();
});

// Tambahkan event listener untuk tombol Register
registerForm.addEventListener('submit', (event) => {
  event.preventDefault();
  registerUser();
});

// Tampilkan halaman login dan sembunyikan halaman register
function showLoginPage() {
  loginPage.classList.add('active');
  registerPage.classList.remove('active');
}

// Tampilkan halaman register dan sembunyikan halaman login
function showRegisterPage() {
  registerPage.classList.add('active');
  loginPage.classList.remove('active');
}

// Event listener untuk link Register
registerLink.addEventListener('click', () => {
  showRegisterPage();
});

// Event listener untuk link Login
loginLink.addEventListener('click', () => {
  showLoginPage();
});

// Fungsi untuk mengirim permintaan HTTP ke endpoint login
function loginUser() {
  const email = loginForm.querySelector('input[type="email"]').value;
  const password = loginForm.querySelector('input[type="password"]').value;

  // Kirim permintaan HTTP POST ke endpoint login di backend Adonis.js
  fetch('http://localhost:3333/api/v1/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ email, password }),
  })
    .then((response) => response.json())
    .then((data) => {
      console.log('Login success:', data);
      // Tambahkan logika atau tindakan yang sesuai setelah login berhasil
    })
    .catch((error) => {
      console.error('Login error:', error);
      // Tambahkan logika atau tindakan yang sesuai jika login gagal
    });
}

// Fungsi untuk mengirim permintaan HTTP ke endpoint register
function registerUser() {
  const username = registerForm.querySelector('input[type="text"]').value;
  const email = registerForm.querySelector('input[type="email"]').value;
  const password = registerForm.querySelector('input[type="password"]').value;

  // Kirim permintaan HTTP POST ke endpoint register di backend Adonis.js
  fetch('http://localhost:3333/api/v1/register', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ username, email, password }),
  })
    .then((response) => response.json())
    .then((data) => {
      console.log('Register success:', data);
      // Tambahkan logika atau tindakan yang sesuai setelah register berhasil
    })
    .catch((error) => {
      console.error('Register error:', error);
      // Tambahkan logika atau tindakan yang sesuai jika register gagal
    });
}
```
Program dimulai dengan menampilkan halaman login dan menyembunyikan halaman register secara default dengan memanggil method showLoginPage(). Terdapat event listener untuk tombol dan register dengan aksi click pada button login yang akan memanggil method loginuser() dan aksi click pada button register yang akan memanggil method registeruser() untuk mengirimkan HTTP POST ke endpoint login serta register pada backend AdonisJS.

# 9. Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube

# 10. BONUS !!! Mendemonstrasikan penggunaan Machine Learning
