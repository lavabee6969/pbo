# 1. Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat.

```java
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MainSpotify {
    private static ArrayList<RegisterLogin> registerUser = new ArrayList<>();
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean exit = false;

        while (!exit) {
            System.out.println("1. Register");
            System.out.println("2. Login");
            System.out.println("3. Exit");
            System.out.print("Choose an option: ");
            int option = scanner.nextInt();

            switch (option) {
                case 1:
                    register(scanner);
                    break;
                case 2:
                    login(scanner);
                    break;
                case 3:
                    exit = true;
                    System.out.println("Goodbye!");
                    break;
                default:
                    System.out.println("Invalid option. Please try again.");
                    break;
            }

            System.out.println();
        }
    }

    private static void register(Scanner scanner) {
        System.out.print("Enter username: ");
        String username = scanner.next();
        System.out.print("Enter password: ");
        String password = scanner.next();

        // Check if the username is already taken
        for (RegisterLogin registerLogin : registerUser) {
            if (registerLogin.authenticate(username, password)) {
                System.out.println("Username already exists. Please choose a different username.");
                return;
            }
        }

        User user = new User(username, password);
        registerUser.add(user);
        System.out.println("User registered successfully!");
    }

    private static void login(Scanner scanner) {
        System.out.print("Enter username: ");
        String username = scanner.next();
        System.out.print("Enter password: ");
        String password = scanner.next();

        for (RegisterLogin user : registerUser) {
            if (user.authenticate(username, password)) {
                System.out.println("Login successful!");
                System.out.println();
                // return;
                
                boolean running = true;
                do {
                    // Buat objek chart musik dan playlist
                    ChartMusic chartMusic = new ChartMusic();
                    Playlist playlist = new Playlist();

                    while (running) {
                        System.out.println("Welcome to Spotify");
                        System.out.println("1. Music Chart");
                        System.out.println("2. Show playlist");
                        System.out.println("3. Search");
                        System.out.println("4. Exit");
                        System.out.print("Choose an option: ");
                        int choice = scanner.nextInt();
                        System.out.println();

                        switch (choice) {
                            case 1:
                            chartMusic.showSongs();
                            System.out.print("Choose a song: ");
                            int songIndex = scanner.nextInt();

                            // Tambahkan lagu ke playlist
                            Song chosenSong = chartMusic.getSong(songIndex);
                            playlist.addSong(chosenSong);
                            System.out.println("Song added to your playlist");
                            
                            // Putar Musik
                            System.out.print("Play Song (Y/N) : ");
                            playlist.play();
                            break;

                            case 2:
                            playlist.showSongs();
                            System.out.print("Enter song number to remove: ");
                            int songNumber = scanner.nextInt();
                            playlist.removeSong(songNumber);
                            break;
                            
                            case 3:
                            searchSongs(scanner, chartMusic, playlist);

                            // Putar Musik
                            System.out.print("Play Song (Y/N) : ");
                            playlist.play();
                            break;

                            case 4:
                            running = false;
                            break;

                            default:
                            System.out.println("Invalid choice. Please choose again.");
                        }
                        System.out.println();
                    }
                } while (running);      
            } 
            else {
                System.out.println("Invalid username or password. Please try again.");
            }
        }
    }

    private static void searchSongs(Scanner scanner, ChartMusic chartMusic, Playlist playlist) {
        System.out.println("Search Songs:");
        System.out.println("1. Search by Title");
        System.out.println("2. Search by Artist");
        System.out.print("Choose an option: ");
        int searchOption = scanner.nextInt();
        scanner.nextLine(); // Clear newline character from input
    
        switch (searchOption) {
            case 1:
                System.out.print("Enter song title: ");
                String title = scanner.nextLine();
                playlist.searchAndAddSongByTitle(title, chartMusic);
                break;
            case 2:
                System.out.print("Enter artist name: ");
                String artist = scanner.nextLine();
                playlist.searchAndAddSongsByArtist(artist, chartMusic);
                break;
            default:
                System.out.println("Invalid option. Please try again.");
                break;
        }
    }
}
```
Dengan konsep Object Oriented Programming, program ini menggunakan pendekatan objek beserta pengoperasian pada Menu utama dalam memberikan layanan musik digital kepada pengguna. Para pengguna dapat melakukan berbagai operasi serta menikmati layanan yang tersedia seperti Melihat Chart Music, Mendengarkan Music, Menambahkan dan menghapus lagu dalam Playlist, serta Melakukan Pencaharian lagu berdasarkan judul ataupun artist yang diinginkan. Program ini terdiri dari kelas-kelas yang merepresentasikan pengoperasian layanan music yang digunakan beserta dengan metode yang digunakan. Tidak hanya itu Program ini menggunakan objek Scanner untuk menerima input dari pengguna melalui keyboard.

Bagian tersulit dari program ini ialah pada bagian program utama atau main menu yang di representasikan melalui public class MainSpotify. Dengan banyaknya methode dari berbagai kelas, terdapat kesulitan dalam memanggil methode saat pembuatan objek yang terkadang membuat kekeliruaan programmer.

# 2. Mampu menjelaskan algoritma dari solusi yang dibuat
1. Program akan menjalankan method **main()** pada class MainSpotify
2. Dalam method **main()** terdapat **loop while** yang akan terus berjalan selama variabel exit bervalue false
3. Pada setiap iterasi loop, program menampilkan menu opsi **register, login, dan exit**.
4. Jika user memilih opsi **register**, program akan memanggil method **register(Scanner scanner)** untuk melakukan proses registrasi. Method ini meminta user untuk memasukan **username** dan **password**.
5. Jika **username** sudah digunakan, akan menampilkan pesan kesalahan. Jika **username ** baru, program akan membuat objek user baru dan ditambahkan kedalam **list registerUser**
6. Jika user memilih opsi **login**, program memanggil method **login(Scanner scanner)** untuk melakukan proses login. Method ini meminta user untuk memasukkan **username** dan **password**, lalu diperiksa data dengan memanggil method **authenticate()** pada setiap objek **RegisterLogin** dalam **list registerUser**. Jika data valid, user berhasil login dan program akan menampilkan menu utama.
7. Setelah berhasil login, program menampilkan menu utama berupa **Music Chart**, **Show Playlist**, **Search**, dan **Exit**.
8. Jika user memilih opsi **Music Chart**, program membuat objek ChartMusic dan memanggil method **showSongs()** untuk menampilkan daftar lagu yang ada di chart musik
9. Program kemudian meminta user untuk memilih lagu yang ingin ditambahkan ke dalam playlist dengan memasukkan indeks lagu. Setelah memilih lagu, program memanggil method **addSong()** dari objek Playlist untuk menambahkan lagu ke dalam playlist.
10. Program akan menawarkan user untuk memutar lagu atau tidak, jika user memilih opsi "Y" maka program akan memanggil method **play()** untuk menampilkan informasi lagu sedang diputar, jika user memilih opsi"N" maka program akan kembali ke menu utama.
11. Jika user memilih opsi **Show Playlist**, program memanggil method **showSongs()** dari objek Playlist untuk menampilkan daftar lagu yang ada dalam playlist.
12. Jika user memilih nomor lagu, dan memasukan input "Y", maka lagu yang dipilih akan terhapus dari playlist menggunakan method **removeSong()** sesuai dengan songNumber yang dpilih. Sebaliknya jika user memasukan input "N", maka program akan kembali menampilkan menu.
13. Jika user memilih **opsi Search**, program memanggil method **searchSongs()** untuk melakukan pencarian lagu berdasarkan judul atau artis. Program meminta user untuk memilih opsi pencarian, lalu meminta input judul atau artis dari user. Setelah itu, program memanggil method **searchAndAddSongByTitle()** atau **searchAndAddSongsByArtist()** dari objek Playlist untuk mencari lagu sesuai dengan input user dan menambahkannya ke dalam playlist.
14. Jika pengguna memilih opsi **Exit**, variabel exit diubah menjadi **true** untuk keluar dari **loop while**.
15.	Program berakhir.

# 3. Mampu Menjelaskan konsep dasar OOP
Object Oriented Programming (OOP) adalah sebuah pendekatan dalam pemrograman dengan menggunakan konsep objek yang memiliki atribut(karakteristik) dan metode(perilaku) untuk mengorganisasikan sebuah program.

Konsep Dasar OOP terdiri dari 4 Pilar, diantarannya :
1. Abstraksi : Kerangka dasar yang memiliki kemampuan untuk menyembunyikan detail pengoperasian suatu objek dan hanya memberikan informasi yang lebih sederhana.
2. Enkapsulasi : Menggabungkan data dan methode yang beroperasi pada data dalam satu entitas (objek)dengan memberi pembatasan akses data.
3. Inheritance : Penurunan sifat suatu objek berupa penurunan atribut atau metode yang sama kepada objek baru.
4. Polymorphism : kemampuan objek untuk memiliki banyak bentuk atau perilaku yang berbeda.

Sebagai Programmer, OOP menjadi salah satu cara atau paradigma yang sangat penting digunakan dalam industri, karena beberapa hal, yaitu :
1. Reusability dan Skalability : Objek yang dibuat dalam program OOP dapat digunakan kembali untuk berbagai project dan program dapat diperluas dengan menambahkan objek baru atau mengganti objek. Sehingga dapat menghemat waktu dan upaya dalam pengembangan perangkat lunak.
2. Mudah dimengerti : Pendeklarasian dalam program OOP dibuat semudah mengkin dengan mencerminkan entitas dunia nyata (kehidupan sehari-hari) atau konsep yang lebih abstract dalam pembuatan objek. Dengan penggunaan konsep 4 pilar hal ini mempermudah programmer dalam membaca dan memahami kode.
3. Modularitas : Dengan membagi program menjadi bagian-bagian yang lebih kecil (objek) dengan fungsi dan data member yang berbeda. OOP memungkinkan programmer untuk memecah masalah yang kompleks menjadi bagian bagian kecil yang mudah di maintenance sehingga pendekatan ini mempermudah dalam penghematan waktu dan usaha.

Ketiga hal ini, sangat membantu dalam pembuatan program produk digital Spotify ini dan menjawab berbagai kesulitan yang dialami selama proses coding seperti pada bagian kesulitan soal nomor 1.

# 4. Mampu mendemonstrasikan penggunaan Encapsulation secara tepat
```java
class RegisterLogin {
    private String username;
    private String password;

    // paramereter
    public RegisterLogin(String username, String password) {
        this.username = username;
        this.password = password;
    }

    // Getter Username
    public String getUsername() {
        return username;
    }

    // Setter Username
    public void setUsername(String username) {
        this.username = username;
    }

    // Getter Password
    public String getPassword() {
        return password;
    }

    // Setter Password
    public void setPassword(String password) {
        this.password = password;
    }
}
```
```java
class ChartMusic extends MusicService {
    private ArrayList<Song> songs = new ArrayList<>();

    // Konstruktor ChartMusic, tambahkan lagu-lagu ke list
    public ChartMusic() {
        songs.add(new Song("Super", "Seventeen"));
        songs.add(new Song("Levitating", "Dua Lipa"));
        songs.add(new Song("Good 4 U", "Olivia Rodrigo"));
        songs.add(new Song("Limbo", "Keshi"));
        songs.add(new Song("Separuh Nafas", "Dewa 19"));
        songs.add(new Song("Bahagia", "GAC"));
        songs.add(new Song("Butter", "BTS"));
        songs.add(new Song("OMG", "New Jeans"));
        songs.add(new Song("Kill Bill", "SZA"));
    }

    // Method untuk menampilkan daftar lagu
    public void showSongs() {
        System.out.println("Chart Music:");
        for (int i = 0; i < songs.size(); i++) {
            System.out.println((i+1) + ". " + songs.get(i));
        }
    }

    // Method untuk mendapatkan lagu berdasarkan index
    public Song getSong(int index) {
        return songs.get(index - 1);
    }

    // Method untuk mendapatkan semua lagu
    public ArrayList<Song> getSongs() {
        return songs;
    }
}
```
```java
class Playlist extends MusicService {
    private ArrayList<Song> songs = new ArrayList<>();

    // Method untuk menambahkan lagu ke playlist
    public void addSong(Song song) {
            songs.add(song);
    }

    // Method untuk menambahkan beberapa lagu ke playlist
    public void addSongs(List<Song> songList) {
        songs.addAll(songList);
    }

    // Method untuk menampilkan daftar lagu dalam playlist
    public void showSongs() {
        if (songs.isEmpty()) {
            System.out.println("Your playlist is empty");
        } else {
            System.out.println("Your playlist:");
            System.out.println("=================================");
            for (int i = 0; i < songs.size(); i++) {
                System.out.println((i+1) + ". " + songs.get(i));
                System.out.println("=================================");
            }
        }
    }

    public void play() {
        Scanner input = new Scanner(System.in);
        char pilih = input.next().charAt(0);
        if (pilih == 'Y') {
            int i = songs.size() - 1;
            System.out.println("Now playing: " + songs.get(i));
        } 
        else if (pilih == 'N') {
            
        }
        else {
            System.out.println("Invalid Choice");
        }
        
    }

    public void removeSong(int songNumber) {
        Scanner input = new Scanner(System.in);
        System.out.print("Do you want to remove song from playlist (Y/N) ? : ");
        char pilih = input.next().charAt(0);
        if (pilih == 'Y') {
            if (songNumber >= 1 && songNumber <= songs.size()) {
                Song removedSong = songs.remove(songNumber - 1);
                System.out.println("Removed song from the playlist: " + removedSong.getTitle());
            } else {
                System.out.println("Invalid song number");
            }
        }
        else if (pilih == 'N') {
            System.out.println("Song removal cancelled.");
        }
        else {
            System.out.println("Invalid Choice");
        }
    }
    
     // Method untuk mencari dan menambahkan lagu ke playlist berdasarkan judul
     public void searchAndAddSongByTitle(String title, ChartMusic chartMusic) {
        Song foundSong = SearchMusic.searchSongByTitle(title, chartMusic);
        if (foundSong != null) {
            addSong(foundSong);
            System.out.println("Title: " + foundSong.getTitle());
            System.out.println("Artist: " + foundSong.getArtist());
            System.out.println("Song added to your playlist");
        } else {
            System.out.println("Song not found in the chart");
        }
    }

    // Method untuk mencari dan menambahkan lagu ke playlist berdasarkan artis
    public void searchAndAddSongsByArtist(String artist, ChartMusic chartMusic) {
        List<Song> foundSongs = SearchMusic.searchSongsByArtist(artist, chartMusic);
        if (!foundSongs.isEmpty()) {
            addSongs(foundSongs);
            for (Song song : foundSongs) {
                System.out.println("Title: " + song.getTitle());
                System.out.println("Artist: " + song.getArtist());
                System.out.println("---------------------------");
            }
            System.out.println("Songs added to your playlist");
        } else {
            System.out.println("No songs found in the chart for the given artist");
        }
    }
}
```
```java
class Song {
    private String title;
    private String artist;

    // Konstruktor Song
    public Song(String title, String artist) {
        this.title = title;
        this.artist = artist;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    @Override
    public String toString() {
        return title + " - " + artist;
    }
}
```
Kelas-kelas tersebut mengimplementasikan enkapsulasi dengan menggunakan access modifier (private dan public) pada atribut-atribut tertentu untuk mengontrol akses dan memastikan bahwa data hanya diakses dan dimodifikasi melalui metode-metode yang disediakan oleh kelas tersebut. Hal ini membantu dalam menjaga keamanan dan integritas data dalam program.

# 5. Mampu mendemonstrasikan penggunaan Abstraction secara tepat
```java
abstract class MusicService {
    public void showSongs() {

    }

    public void addSongs() {

    }

    public void addSong() {

    }
}
```
Kelas abstrak MusicService memberikan kerangka kerja umum untuk fitur-fitur yang dimiliki oleh layanan musik, seperti menambahkan lagu dan menampilkan daftar lagu. Kelas-kelas turunan, seperti ChartMusic dan Playlist, mengimplementasikan metode-metode abstrak tersebut sesuai dengan kebutuhan.

# 6. Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat
## Inheritance 
```java
abstract class MusicService {
    public void showSongs() {

    }

    public void addSongs() {

    }

    public void addSong() {

    }
}

// Kelas ChartMusic merepresentasikan chart musik
class ChartMusic extends MusicService {
    private ArrayList<Song> songs = new ArrayList<>();

    // Konstruktor ChartMusic, tambahkan lagu-lagu ke list
    public ChartMusic() {
        songs.add(new Song("Super", "Seventeen"));
        songs.add(new Song("Levitating", "Dua Lipa"));
        songs.add(new Song("Good 4 U", "Olivia Rodrigo"));
        songs.add(new Song("Limbo", "Keshi"));
        songs.add(new Song("Separuh Nafas", "Dewa 19"));
        songs.add(new Song("Bahagia", "GAC"));
        songs.add(new Song("Butter", "BTS"));
        songs.add(new Song("OMG", "New Jeans"));
        songs.add(new Song("Kill Bill", "SZA"));
    }

    // Method untuk menampilkan daftar lagu
    public void showSongs() {
        System.out.println("Chart Music:");
        for (int i = 0; i < songs.size(); i++) {
            System.out.println((i+1) + ". " + songs.get(i));
        }
    }

    // Method untuk mendapatkan lagu berdasarkan index
    public Song getSong(int index) {
        return songs.get(index - 1);
    }

    // Method untuk mendapatkan semua lagu
    public ArrayList<Song> getSongs() {
        return songs;
    }
}

// Kelas Playlist merepresentasikan playlist user
class Playlist extends MusicService {
    private ArrayList<Song> songs = new ArrayList<>();

    // Method untuk menambahkan lagu ke playlist
    public void addSong(Song song) {
            songs.add(song);
    }

    // Method untuk menambahkan beberapa lagu ke playlist
    public void addSongs(List<Song> songList) {
        songs.addAll(songList);
    }

    // Method untuk menampilkan daftar lagu dalam playlist
    public void showSongs() {
        if (songs.isEmpty()) {
            System.out.println("Your playlist is empty");
        } else {
            System.out.println("Your playlist:");
            System.out.println("=================================");
            for (int i = 0; i < songs.size(); i++) {
                System.out.println((i+1) + ". " + songs.get(i));
                System.out.println("=================================");
            }
        }
    }

    public void play() {
        Scanner input = new Scanner(System.in);
        char pilih = input.next().charAt(0);
        if (pilih == 'Y') {
            int i = songs.size() - 1;
            System.out.println("Now playing: " + songs.get(i));
        } 
        else if (pilih == 'N') {
            
        }
        else {
            System.out.println("Invalid Choice");
        }
        
    }

    public void removeSong(int songNumber) {
        Scanner input = new Scanner(System.in);
        System.out.print("Do you want to remove song from playlist (Y/N) ? : ");
        char pilih = input.next().charAt(0);
        if (pilih == 'Y') {
            if (songNumber >= 1 && songNumber <= songs.size()) {
                Song removedSong = songs.remove(songNumber - 1);
                System.out.println("Removed song from the playlist: " + removedSong.getTitle());
            } else {
                System.out.println("Invalid song number");
            }
        }
        else if (pilih == 'N') {
            System.out.println("Song removal cancelled.");
        }
        else {
            System.out.println("Invalid Choice");
        }
    }
    
     // Method untuk mencari dan menambahkan lagu ke playlist berdasarkan judul
     public void searchAndAddSongByTitle(String title, ChartMusic chartMusic) {
        Song foundSong = SearchMusic.searchSongByTitle(title, chartMusic);
        if (foundSong != null) {
            addSong(foundSong);
            System.out.println("Title: " + foundSong.getTitle());
            System.out.println("Artist: " + foundSong.getArtist());
            System.out.println("Song added to your playlist");
        } else {
            System.out.println("Song not found in the chart");
        }
    }

    // Method untuk mencari dan menambahkan lagu ke playlist berdasarkan artis
    public void searchAndAddSongsByArtist(String artist, ChartMusic chartMusic) {
        List<Song> foundSongs = SearchMusic.searchSongsByArtist(artist, chartMusic);
        if (!foundSongs.isEmpty()) {
            addSongs(foundSongs);
            for (Song song : foundSongs) {
                System.out.println("Title: " + song.getTitle());
                System.out.println("Artist: " + song.getArtist());
                System.out.println("---------------------------");
            }
            System.out.println("Songs added to your playlist");
        } else {
            System.out.println("No songs found in the chart for the given artist");
        }
    }

    
}
```
```java
class RegisterLogin {
    private String username;
    private String password;

    // paramereter
    public RegisterLogin(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    // method
    public boolean authenticate(String username, String password) {
        return this.username.equals(username) && this.password.equals(password);
    }

    public boolean authenticate() {
        return false; // Implementasi default, selalu mengembalikan false
    }
}

// class turunan
class User extends RegisterLogin {
    public User(String username, String password) {
        super(username, password);
    }

    @Override
    public boolean authenticate() {
        return true; // Implementasi overriding, selalu mengembalikan true
    }
}
```
Dalam Program ini, kelas RegisterLogin adalah kelas induk (Superclass) dengan atribut username dan password serta method authenticate(). Sedangkan Kelas User merupakan kelas anak (Subclass) dari RegisterLogin yang mewarisi atribut serta method dari kelas RegisterLogin.

Kelas MusicService sebagai kelas abstrak memberikan kerangka dasar metode yang harus diimplementasikan oleh kelas turunannya, yaitu ChartMusic dan Playlist. ChartMusic mengimplementasikan metode showSongs() untuk menampilkan daftar lagu dalam chart musik, sedangkan Playlist mengimplementasikan metode addSong(), addSongs(), showSongs(), play(), dan removeSong() yang berhubungan dengan manajemen playlist.

## Polymorphism
```java
class RegisterLogin {
    private String username;
    private String password;

    // method
    public boolean authenticate(String username, String password) {
        return this.username.equals(username) && this.password.equals(password);
    }

    public boolean authenticate() {
        return false; // Implementasi default, selalu mengembalikan false
    }
}

// class turunan
class User extends RegisterLogin {
    public User(String username, String password) {
        super(username, password);
    }

    @Override
    public boolean authenticate() {
        return true; // Implementasi overriding, selalu mengembalikan true
    }
}
```
Kelas RegisterLogin memiliki metode authenticate() yang mengembalikan nilai false secara default. Kelas turunan User meng-override metode authenticate() tersebut dan mengubah perilakunya untuk selalu mengembalikan nilai true.

```java
abstract class MusicService {
    public void showSongs() {

    }
}

class ChartMusic extends MusicService {
    // Method untuk menampilkan daftar lagu
    public void showSongs() {
        System.out.println("Chart Music:");
        for (int i = 0; i < songs.size(); i++) {
            System.out.println((i+1) + ". " + songs.get(i));
        }
    }
}

class Playlist extends MusicService {
    // Method untuk menampilkan daftar lagu dalam playlist
    public void showSongs() {
        if (songs.isEmpty()) {
            System.out.println("Your playlist is empty");
        } else {
            System.out.println("Your playlist:");
            System.out.println("=================================");
            for (int i = 0; i < songs.size(); i++) {
                System.out.println((i+1) + ". " + songs.get(i));
                System.out.println("=================================");
            }
        }
    }
}
```
Kelas MusicService memiliki metode showSongs() yang bersifat abstrak. Kelas turunan ChartMusic dan Playlist mengimplementasikan metode showSongs() sesuai dengan fungsionalitas yang diperlukan oleh kelas tersebut.

# 7. Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat
Proses bisnis yang diimplementasikan berdasar pada use case utama yang dioperasikan didalam progam ini yaitu layanan akun serta layanan pemutaran, pencaharian, dan pembuatan playlist:

1. **Layanan Account**

Use Case            : Manage Account

Hubungan Entitas    : Class RegisterLogin

Method              : public boolean authenticate()

2. **Memutar Lagu**

Use Case            : Play Music

Hubungan Entitas    : Class Playlist extends MusicService

Method              : public void play()

3. **Pencaharian Lagu**

Use Case            : Browse Songs

Hubungan Entitas    : Class SearchMusic

Method              : public static searchSongByTitle(), public static searchSongsByArtist()

4. **Menambahkan lagu ke dalam Playlist**

Use Case            : Add to Playlist

Hubungan Entitas    : Class Playlist extends MusicService

Method              : public void addSong(), public void addSongs()

5. **Menghapus lagu dari Playlist**

Use Case            : Remove from Playlist

Hubungan Entitas    : Class Playlist extends MusicService

Method              : public void removeSong()

**Identifikasi entitas utama:**
Entitas utama dalam proses bisnis ini adalah User, ChartMusic, Playlist, dan Song.

**Definisikan kelas-kelas:**
kelas-kelas yang mewakili entitas-entitas tersebut: RegisterLogin, User, MusicService (kelas abstrak), ChartMusic, Playlist, SearchMusic, dan Song.

**Identifikasi hubungan antar entitas:**
User memiliki hubungan dengan RegisterLogin (kelas turunan).
ChartMusic dan Playlist merupakan turunan dari kelas abstrak MusicService.
Playlist memiliki hubungan dengan Song.

**Definisikan atribut dan metode:**
Setiap kelas memiliki atribut dan metode yang relevan dengan entitas yang diwakilinya. Atribut dan metode yang sudah didefinisikan dalam contoh program dapat digunakan sebagai referensi.

**Implementasikan metode:**
Implementasikan kode untuk setiap metode yang ada dalam kelas-kelas tersebut. Metode-metode ini mencerminkan perilaku yang diharapkan sesuai dengan proses bisnis yang ingin diwakili.

**Pembuatan objek:**
Buat objek-objek dari kelas-kelas yang telah didefinisikan. Objek-objek ini akan mewakili instance konkret dari entitas-entitas dalam proses bisnis. Objek RegisterLogin, User, ChartMusic, dan Playlist dapat dibuat sesuai dengan logika program yang sudah ada.

**Gunakan objek untuk menjalankan proses bisnis:**
Gunakan objek-objek yang telah dibuat untuk menjalankan proses bisnis. Panggil metode-metode yang sesuai pada objek-objek tersebut untuk melakukan operasi yang diperlukan.

# 8. Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table
- Use Case Table
![](https://gitlab.com/lavabee6969/pbo/-/raw/main/Jawaban/Screenshot_2023-06-05_173132.png)

- Class Diagram
![](https://gitlab.com/lavabee6969/pbo/-/raw/main/Jawaban/Screenshot_2023-05-17_133526.png)

# 9. Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video
https://youtu.be/NjI8mehAz84

# 10. Inovasi UX
Registrasi akun
![](https://gitlab.com/lavabee6969/pbo/-/raw/main/Jawaban/Registrasi.png)

Login

![](https://gitlab.com/lavabee6969/pbo/-/raw/main/Jawaban/Login.png)


Memutar lagu dan Menambahkan lagu kedalam Playlist
![](https://gitlab.com/lavabee6969/pbo/-/raw/main/Jawaban/1.png)
![](https://gitlab.com/lavabee6969/pbo/-/raw/main/Jawaban/2.png)

Melihat dan menghapus daftar lagu dalam Playlist
![](https://gitlab.com/lavabee6969/pbo/-/raw/main/Jawaban/Screenshot_2023-05-23_150909.png)
![](https://gitlab.com/lavabee6969/pbo/-/raw/main/Jawaban/Screenshot_2023-05-23_151012.png)

Mencari lagu berdasarkan judul dan artist
![](https://gitlab.com/lavabee6969/pbo/-/raw/main/Jawaban/search.png)
![](https://gitlab.com/lavabee6969/pbo/-/raw/main/Jawaban/search2.png)

Keluar

![](https://gitlab.com/lavabee6969/pbo/-/raw/main/Jawaban/exit.png)
